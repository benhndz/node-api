import Sequelize from 'sequelize';
import dotEnv from 'dotenv';

dotEnv.config();

const DB_URL = `postgresql://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_DATABASE}`;
const sequelize = new Sequelize(DB_URL);

export default sequelize;
