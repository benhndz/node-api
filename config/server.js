import path from 'path';
import { fileURLToPath } from 'url';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

export default {
  development: {
    sitename: 'Node API [Development]',
    data: {
      avatars: path.join(__dirname, '../data/avatars'),
    },
  },
  production: {
    sitename: 'Node API',
    data: {
      avatars: path.join(__dirname, '../data/avatars'),
    },
  },
};
