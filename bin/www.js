import dotEnv from 'dotenv';
import serverConfig from '../config/server';
import server from '../server';
import sequelize from '../config/db';

dotEnv.config();

const app = server(serverConfig[process.env.NODE_ENV || 'development']);

const PORT = process.env.PORT || 3000;

sequelize
  .sync()
  .then(() => {
    app.listen(PORT, () => {
      console.log(`Server listening on port ${PORT}`);
    });
    // eslint-disable-next-line no-unused-vars
    app.use((err, req, res, next) => {
      console.error(err.stack);
      res.status(500).send({
        status: 500,
        message: err.message,
        body: {},
      });
    });
  })
  .catch((err) => {
    console.log(err);
  });
