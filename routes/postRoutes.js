import express from 'express';
import {
  createPost,
  getAllPosts,
  getPostById,
  updatePost,
  deletePost,
} from '../controllers/postController';
import {
  validateBody,
  validateQueryParams,
} from '../middleware/schemaValidation';
import validateToken from '../middleware/tokenValidation';
import {
  createPostSchema,
  getAllPostsSchema,
  getPostSchema,
  updatePostSchema,
} from '../apiSchema/postSchema';

const router = express.Router();

export default () => {
  router.post('/', validateToken, validateBody(createPostSchema), createPost);

  router.get(
    '/',
    validateToken,
    validateQueryParams(getAllPostsSchema),
    getAllPosts
  );

  router.get(
    '/:id',
    validateToken,
    validateQueryParams(getPostSchema),
    getPostById
  );

  router.put('/:id', validateToken, validateBody(updatePostSchema), updatePost);

  router.delete(
    '/:id',
    validateToken,
    validateQueryParams(getPostSchema),
    deletePost
  );

  return router;
};
