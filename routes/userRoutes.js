import express from 'express';
import { signup, login } from '../controllers/userController';
import { signupSchema, loginSchema } from '../apiSchema/userSchema';
import { validateBody } from '../middleware/schemaValidation';
import AvatarService from '../services/avatarService';
import { handleAvatar, upload } from '../middleware/imageHandler';

const router = express.Router();

export default (config) => {
  const avatars = new AvatarService(config.data.avatars);

  router.post(
    '/signup',
    upload.single('avatar'),
    validateBody(signupSchema),
    handleAvatar(avatars),
    signup
  );

  router.post('/login', validateBody(loginSchema), login);

  return router;
};
