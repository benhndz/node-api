FROM node:15-alpine

WORKDIR /home/ben/api

COPY package.json /home/ben/api

RUN npm install

COPY . /home/ben/api

EXPOSE 3000

CMD [ "npm", "start"]

