export const defaultServerResponse = {
  status: 400,
  message: '',
  body: {},
};

export const postMessage = {
  POST_CREATED: 'Post Created Succefully',
  POST_FETCHED: 'Post Fetched Succefully',
  POST_UPDATED: 'Post Updated Succefully',
  POST_DELETED: 'Post Deleted Succefully',
  POST_NOT_FOUND: 'Post Not Found',
};

export const userMessage = {
  SIGNUP_SUCCESS: 'Signup Success',
  LOGIN_SUCCESS: 'Login Success',
  DUPLICATE_USER: 'User already exist',
  USER_NOT_FOUND: 'User Not Found',
  INVALID_PASSWORD: 'Incorrect password',
  MISSING_AVATAR: 'Missing Avatar',
};

export const requestValidationMessage = {
  BAD_RESQUEST: 'Invalid fields',
  TOKEN_MISSING: 'Missing Token',
};

export const databaseMessage = {
  INVALID_ID: 'Invalid Id',
};
