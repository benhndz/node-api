import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import rateLimit from 'express-rate-limit';

import userRoutes from './routes/userRoutes';
import postRoutes from './routes/postRoutes';

import User from './models/user';
import Post from './models/post';

export default (config) => {
  const app = express();

  // security
  app.use(helmet());

  // Limit request
  const limiter = rateLimit({
    windowMs: 1 * 60 * 1000, // 1 minute
    max: 5, // 5 requests,
  });
  app.use(limiter);

  // cors
  app.use(cors());

  // request payload middleware
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));

  // Sequelize associations
  Post.belongsTo(User, { contraints: true, onDelete: 'CASCADE' });
  User.hasMany(Post);

  // Routes
  app.use('/api/v1/user', userRoutes(config));
  app.use('/api/v1/posts', postRoutes(config));

  app.get('/', (req, res) => {
    res.send('Hello from NODE-API server');
  });

  return app;
};
