import Joi from 'joi';

export const createPostSchema = Joi.object({
  title: Joi.string().required(),
  description: Joi.string().required(),
});

export const getAllPostsSchema = Joi.object({
  skip: Joi.string(),
  limit: Joi.string(),
});

export const getPostSchema = Joi.object({
  id: Joi.string(),
});

export const updatePostSchema = Joi.object({
  title: Joi.string(),
  description: Joi.string(),
});
