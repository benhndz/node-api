describe('Initial test', () => {
  it('true equals true', () => {
    expect(true).toBe(true);
  });
  it('false equals false', () => {
    expect(false).toBe(false);
  });
});
