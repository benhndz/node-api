import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { userMessage } from '../constants';
import User from '../models/user';

class UserService {
  static async signup({ username, email, password, file, avatars }) {
    try {
      const user = await User.findOne({ where: { username: username } });
      if (user) {
        throw new Error(userMessage.DUPLICATE_USER);
      }
      password = await bcrypt.hash(password, 12);
      if (!file) {
        throw new Error(userMessage.MISSING_AVATAR);
      }
      const avatar = file.storedFilename;
      const newUser = await User.create({ username, email, password, avatar });
      return newUser;
    } catch (error) {
      if (file && file.storedFilename) {
        await avatars.delete(file.storedFilename);
      }
      console.log('Something went wrong: Service: signup', error);
      throw new Error(error);
    }
  }

  static async login({ email, password }) {
    try {
      const user = await User.findOne({ where: { email: email } });
      if (!user) {
        throw new Error(userMessage.USER_NOT_FOUND);
      }
      const isValid = await bcrypt.compare(password, user.password);
      if (!isValid) {
        throw new Error(userMessage.INVALID_PASSWORD);
      }
      const token = jwt.sign(
        { id: user.id },
        process.env.SECRET_KEY || 'my-secret-key',
        { expiresIn: '1d' }
      );
      return { token };
    } catch (error) {
      console.log('Something went wrong: Service: login', error);
      throw new Error(error);
    }
  }
}

export default UserService;
