import sharp from 'sharp';
import { uuid } from 'uuidv4';
import util from 'util';
import path from 'path';
import fs from 'fs';

const fsunlink = util.promisify(fs.unlink);

class AvatarService {
  constructor(directory) {
    this.directory = directory;
  }

  async store(buffer) {
    const filename = AvatarService.filename();
    const filepath = this.filepath(filename);

    await sharp(buffer)
      .resize(300, 300, {
        fit: sharp.fit.inside,
        withoutEnlargement: true,
      })
      .toFile(filepath);
    return filename;
  }

  //   async thumbnail(filename) {
  //     return sharp(this.filepath(filename)).resize(50, 50).toBuffer();
  //   }

  async delete(filename) {
    return fsunlink(this.filepath(filename));
  }

  static filename() {
    return `${uuid()}.png`;
  }

  filepath(filename) {
    return path.resolve(`${this.directory}/${filename}`);
  }
}

export default AvatarService;
