import { postMessage } from '../constants';
import Post from '../models/post';

class PostService {
  static async createPost(user, body) {
    const { title, description } = body;
    try {
      const post = await user.createPost({
        title,
        description,
      });
      return post;
    } catch (error) {
      console.log('Something went wrong: Service: createPost', error);
      throw new Error(error);
    }
  }

  static async getAllPosts({ offset = 0, limit = 10 }) {
    try {
      const posts = await Post.findAll({
        order: [['id', 'ASC']],
        offset,
        limit,
      });
      return posts;
    } catch (error) {
      console.log('Something went wrong: Service: getAllPosts', error);
      throw new Error(error);
    }
  }

  static async getPostById({ id }) {
    try {
      const post = await Post.findByPk(id);
      if (!post) {
        throw new Error(postMessage.POST_NOT_FOUND);
      }
      return post;
    } catch (error) {
      console.log('Something went wrong: Service: getPostById', error);
      throw new Error(error);
    }
  }

  static async updatePost({ id, updateInfo }) {
    try {
      const { title, description } = updateInfo;
      const post = await Post.findByPk(id);
      if (!post) {
        throw new Error(postMessage.POST_NOT_FOUND);
      }
      post.title = title || post.title;
      post.description = description || post.description;
      const updatePost = post.save();
      return updatePost;
    } catch (error) {
      console.log('Something went wrong: Service: updatePost', error);
      throw new Error(error);
    }
  }

  static async deletePost({ id }) {
    try {
      const post = await Post.findByPk(id);
      if (!post) {
        throw new Error(postMessage.POST_NOT_FOUND);
      }
      post.destroy();
    } catch (error) {
      console.log('Something went wrong: Service: deleteProduct', error);
      throw new Error(error);
    }
  }
}

export default PostService;
