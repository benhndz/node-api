import { defaultServerResponse, postMessage } from '../constants';
import PostService from '../services/postService';

const createPost = async (req, res) => {
  const response = { ...defaultServerResponse };
  try {
    const responseFromService = await PostService.createPost(
      req.user,
      req.body
    );
    response.status = 200;
    response.message = postMessage.POST_CREATED;
    response.body = responseFromService;
  } catch (error) {
    console.log('Something went wrong: Controller: createPost', error);
    response.message = error.message;
  }
  return res.status(response.status).send(response);
};

const getAllPosts = async (req, res) => {
  const response = { ...defaultServerResponse };
  try {
    const responseFromService = await PostService.getAllPosts(req.query);
    response.status = 200;
    response.message = postMessage.POST_FETCHED;
    response.body = responseFromService;
  } catch (error) {
    console.log('Something went wrong: Controller: getAllProducts', error);
    response.message = error.message;
  }
  return res.status(response.status).send(response);
};

const getPostById = async (req, res) => {
  const response = { ...defaultServerResponse };
  try {
    const responseFromService = await PostService.getPostById(req.params);
    response.status = 200;
    response.message = postMessage.POST_FETCHED;
    response.body = responseFromService;
  } catch (error) {
    console.log('Something went wrong: Controller: getPostById', error);
    response.message = error.message;
  }
  return res.status(response.status).send(response);
};

const updatePost = async (req, res) => {
  const response = { ...defaultServerResponse };
  try {
    const responseFromService = await PostService.updatePost({
      id: req.params.id,
      updateInfo: req.body,
    });
    response.status = 200;
    response.message = postMessage.POST_UPDATED;
    response.body = responseFromService;
  } catch (error) {
    console.log('Something went wrong: Controller: updatePost', error);
    response.message = error.message;
  }
  return res.status(response.status).send(response);
};

const deletePost = async (req, res) => {
  const response = { ...defaultServerResponse };
  try {
    const responseFromService = await PostService.deletePost(req.params);
    response.status = 200;
    response.message = postMessage.POST_DELETED;
    response.body = responseFromService;
  } catch (error) {
    console.log('Something went wrong: Controller: deletePost', error);
    response.message = error.message;
  }
  return res.status(response.status).send(response);
};

export { createPost, getAllPosts, getPostById, updatePost, deletePost };
