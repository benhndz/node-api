import { defaultServerResponse, userMessage } from '../constants';
import UserService from '../services/userService';

const signup = async (req, res) => {
  const response = { ...defaultServerResponse };
  try {
    const responseFromService = await UserService.signup({
      ...req.body,
      file: req.file,
      avatars: req.avatars,
    });
    response.status = 200;
    response.message = userMessage.SIGNUP_SUCCESS;
    response.body = responseFromService;
  } catch (error) {
    console.log('Something went wrong: Controller: signup', error);
    response.message = error.message;
  }
  return res.status(response.status).send(response);
};

const login = async (req, res) => {
  const response = { ...defaultServerResponse };
  try {
    const responseFromService = await UserService.login(req.body);
    response.status = 200;
    response.message = userMessage.LOGIN_SUCCESS;
    response.body = responseFromService;
  } catch (error) {
    console.log('Something went wrong: Controller: login', error);
    response.message = error.message;
  }

  return res.status(response.status).send(response);
};

export { signup, login };
