import { defaultServerResponse, requestValidationMessage } from '../constants';

const validateObjectSchema = (data, schema) => {
  const result = schema.validate(data, { convert: false });
  if (result.error) {
    const errorDetails = result.error.details.map((value) => {
      return {
        error: value.message,
        path: value.path,
      };
    });
    return errorDetails;
  }
  return null;
};

export const validateBody = (schema) => {
  return (req, res, next) => {
    const response = { ...defaultServerResponse };
    const error = validateObjectSchema(req.body, schema);
    if (error) {
      response.body = error;
      response.message = requestValidationMessage.BAD_RESQUEST;
      return res.status(response.status).send(response);
    }
    return next();
  };
};

export const validateQueryParams = (schema) => {
  return (req, res, next) => {
    const response = { ...defaultServerResponse };
    const error = validateObjectSchema(req.query, schema);
    if (error) {
      response.body = error;
      response.message = requestValidationMessage.BAD_RESQUEST;
      return res.status(response.status).send(response);
    }
    return next();
  };
};
