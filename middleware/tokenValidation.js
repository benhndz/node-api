import { defaultServerResponse, requestValidationMessage } from '../constants';
import jwt from 'jsonwebtoken';
import User from '../models/user';

export default async (req, res, next) => {
  const response = { ...defaultServerResponse };
  try {
    if (!req.headers.authorization) {
      throw new Error(requestValidationMessage.TOKEN_MISSING);
    }
    const token = req.headers.authorization.split('Bearer')[1].trim();
    const decoded = jwt.verify(
      token,
      process.env.SECRET_KEY || 'my-secret-key'
    );
    const user = await User.findByPk(decoded.id);
    req.user = user;
    return next();
  } catch (error) {
    console.log('Error', error);
    response.message = error.message;
    response.status = 401;
  }
  return res.status(response.status).send(response);
};
