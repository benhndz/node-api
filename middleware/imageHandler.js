import multer from 'multer';

export const upload = multer({
  limits: {
    fileSize: 4 * 1024 * 1024,
  },
});

export const handleAvatar = (avatars) => async (req, res, next) => {
  if (!req.file) return next();
  if (req.file.mimetype !== 'image/png' && req.file.mimetype !== 'image/jpeg') {
    return next(new Error('File format is not supported'));
  }
  req.file.storedFilename = await avatars.store(req.file.buffer);
  req.avatars = avatars;
  return next();
};
